# Ukrainian translations for sumwars package
# Copyright (C) 2009
# This file is distributed under the same license as the sumwars package.
# Igor Paliychuk <mansonigor@gmail.com>, 2011.
msgid ""
msgstr ""
"Project-Id-Version: 0.5\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2009-03-09 15:15+0100\n"
"PO-Revision-Date: 2011-09-07 20:58+0200\n"
"Last-Translator: Igor Paliychuk <mansonigor@gmail.com>\n"
"Language-Team: \n"
"Language: uk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Poedit-Basepath: .\n"
"X-Poedit-Language: Ukrainian\n"
"X-Poedit-Country: UKRAINE\n"

#: goblins.xml:2
msgid "The goblinslayer"
msgstr "Вбивця Гоблінів"

#: goblins.xml:9
msgid "Quest finished"
msgstr "Квест завершено"

#: goblins.xml:12
msgid "you killed enough goblins"
msgstr "Ви вбили досить гоблінів"

#: goblins.xml:18
msgid "peasant"
msgstr "селянин"

#: goblins.xml:19
msgid "Goblins"
msgstr "Гобліни"

#: goblins.xml:26
msgid "I have killed them all."
msgstr "Я вбив їх усіх."

#: goblins.xml:27
msgid "Wow... you're a real hero, aren't ya?"
msgstr "Нічого собі ... Ви справжній герой, хіба ні?"

#: goblins.xml:30
msgid "Leave me alone and kill some more goblins."
msgstr "Залиш мене у спокої і вбий ще кілька гоблінів."

#: goblins.xml:33
msgid "There are some stupid goblins pissing me off."
msgstr "Є деякі дурні гобліни які мене достають."

#: goblins.xml:34
msgid "If you kill 10 of 'em, I'll give you a precious reward."
msgstr "Якщо ви вб'єте 10 із них, я дам вам дорогоцінну винагороду."

#: goblins.xml:35
msgid "Interested?"
msgstr "Зацікавлені?"

#: goblins.xml:36
msgid "Accept the quest?"
msgstr "Прийняти квест?"

#: goblins.xml:37
msgid "Yes"
msgstr "Так"

#: goblins.xml:38
msgid "No"
msgstr "Ні"

#: goblins.xml:43
msgid "Goblins (reward)"
msgstr "Гобліни (винагорода)"

#: goblins.xml:49
msgid "You want a second reward?"
msgstr "Ви хочете другу винагороду?"

#: goblins.xml:50
msgid "Of course!"
msgstr "Звичайно!"

#: goblins.xml:51
msgid "Then I'll give you this goblin club. Be happy."
msgstr "Тоді я дам вам цю палицю гоблінів. Будьте щасливі."

#: goblins.xml:60
msgid "Nearly done."
msgstr "Майже зроблено."

#: goblins.xml:61
msgid "Have fun then."
msgstr "Тоді повеселися."

#: goblins.xml:90
msgid "region1"
msgstr "регіон1"
